class RouteNames {
  static const splashScreen = '/';

  static const signinScreen = '/signin-screen';
  static const signupScreen = '/signup-screen';

  static const homeScreen = '/home-screen';
  static const createScreen = '/create-screen';
  static const editScreen = '/edit-screen';
}
