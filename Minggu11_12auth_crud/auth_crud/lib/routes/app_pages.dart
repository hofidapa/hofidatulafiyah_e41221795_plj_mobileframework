import 'package:auth_crud/routes/route_names.dart';
import 'package:auth_crud/screens/create_screen.dart';
import 'package:auth_crud/screens/edit_screen.dart';
import 'package:auth_crud/screens/home_screen.dart';
import 'package:auth_crud/screens/signin_screen.dart';
import 'package:auth_crud/screens/signup_screen.dart';
import 'package:auth_crud/screens/splash_screen.dart';
import 'package:get/get.dart';

class AppPages {
  static List<GetPage> pages = [
    GetPage(
      name: RouteNames.splashScreen,
      page: () => const SplashScreen(),
    ),
    GetPage(
      name: RouteNames.signinScreen,
      page: () => const SigninScreen(),
    ),
    GetPage(
      name: RouteNames.signupScreen,
      page: () => const SignupScreen(),
    ),
    GetPage(
      name: RouteNames.homeScreen,
      page: () => const HomeScreen(),
    ),
    GetPage(
      name: RouteNames.createScreen,
      page: () => const CreateScreen(),
    ),
    GetPage(
      name: RouteNames.editScreen,
      page: () => const EditScreen(),
    ),
  ];
}
